# Amazon Transcribe Medical

Amazon extended Amazon Transcribe Service especially for medical speech, the company announced this service at its AWS re:Invent conference. The new transcribing service, **Amazon Transcribe Medical**, is machine learning-powered transcriber that will allow quick dictation by physicians for their clinical notes , transcribing speech into accurate text in real time, without any human intervention, claimed by Amazon.

<p align = center>
    <img src = "images/1.jpg", width="100%" height="80%">
</p>

Unlike other services, there won't be any burden to say things like “comma” or “full stop” during the transcription, they can speak normally during the dictation process. The text can then be fed to streaming systems, including ER systems or other languages services by AWS, like Amazon Comprehend Medical for entity extraction. Amazon Transcribe Medical is HIPAA-eligible and scales with the users’ needs, meaning you’ll only pay for what you actually use and without upfront fees, notes Amazon. From a technical perspective, here is the way it works.


## Scenario

<p align = center>
    <img src = "images/2.png", width="80%" height="80%">
</p>

You can use the WebSocket protocol to open a bi-directional affiliation that's designed to be secure with Amazon Transcribe Medical. You encode the audio stream with event stream encryption, and Amazon Transcribe Medical returns a stream of text in JSON blobs that are encoded exploitation event stream encryption. For additional info, see Event Stream encryption. you'll use the data during this section to make applications exploitation the WebSocket library of your alternative.



## Prerequisite

> An AWS Account with IAM User [click here](https://docs.aws.amazon.com/transcribe/latest/dg/setting-up-ascm-med.html) for more info


### Adding a Policy for WebSocket Requests to Your IAM Role

1. For using the WebSocket protocol for calling Amazon Transcribe Medical, you need to attach the following policy to the AWS Identity and Access Management (IAM) role that makes the request..

	```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "transcribemedicalstreaming",
            "Effect": "Allow",
            "Action": "transcribe:StartMedicalStreamTranscription",
            "Resource": "*"
        }
    ]
}
	```
	
### Creating a Pre-Signed URL

1. You need to construct a URL for your WebSocket request that contains the information required to line up communication between your application and Amazon Transcribe Medical. WebSocket streaming uses the Amazon Signature Version 4 process for signing requests. Signing the request helps to verify the identity of the requester and to protect your audio data in transit. It also protects against potential replay attacks. For more information about Signature Version 4, see signing in aws

	The URL has the following format. Line breaks have been added for readability.
	
	```
	GET https://transcribestreaming.region.amazonaws.com:8443/medical-stream-transcription-websocket
?language-code=languageCode
   &X-Amz-Algorithm=AWS4-HMAC-SHA256
   &X-Amz-Credential=Signature Version 4 credential scope
   &X-Amz-Date=date
   &X-Amz-Expires=time in seconds until expiration
   &X-Amz-Security-Token=security-token
   &X-Amz-Signature=Signature Version 4 signature 
   &X-Amz-SignedHeaders=host
   &media-encoding=mediaEncoding
   &sample-rate=mediaSampleRateHertz
   &session-id=sessionId
   &specialty=PRIMARYCARE
    &type=DICTATION        
            
	```

	For TYPE, it is best to select CONVERSATION if your use case involves multiple speakers engaged in a discussion. An example would be a conversation between a physician and a patient. Select DICTATION if your use case involves a single speaker where a person is dictating speech. An example would be if a physician is dictating medical notes for data entry purposes after a patient encounter.

	Use the following values for the URL parameters:
	- **language-code** – The language code for the input audio. The valid value is en-US.
	- **media-encoding** – The encoding used for the input audio. The valid value is pcm.
	- **sample-rate** – The sample rate of the input audio in hertz. 16,000 Hz or higher sample rates are accepted.
	- **sessionId** – Optional. An identifier for the transcription session. If you don't provide a session ID, Amazon Transcribe Medical generates one for you and returns it in the response.
	- **specialty** – The specialty in the medical domain. Must be PRIMARYCARE.
	- **type** – The type of audio. Must be DICTATION or CONVERSATION.

	The remaining parameters are Signature Version 4 parameters:
	
	- **X-Amz-Algorithm** – The algorithm you're using in the signing process. The only valid value is AWS4-HMAC-SHA256.
	- **X-Amz-Credential** – A string separated by slashes ("/") that is formed by concatenating your access key ID and your credential scope components. Credential scope includes the date in YYYYMMDD format, the AWS Region, the service name, and a special termination string (aws4_request).
	- **X-Amz-Date** – The date and time that the signature was created. Generate the date and time by
following the instructions in Handling Dates in Signature Version 4 in the Amazon Web Services General
Reference.
	- **X-Amz-Expires** – The length of time in seconds until the credentials expire. The maximum value is 300
seconds (5 minutes).
	- **X-Amz-Security-Token** – Optional. A Signature Version 4 token for temporary credentials. If you
specify this parameter, include it in the canonical request. For more information, see [Requesting
Temporary Security Credentials](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_temp_request.html) in the AWS Identity and Access Management User Guide.
	- **X-Amz-Signature** – The Signature Version 4 signature that you generated for the request
	- **X-Amz-SignedHeaders** – The headers that are signed when creating the signature for the request. The
only valid value is host.

To construct the URL for the request and create the Signature Version 4 signature, use the following steps. The examples are in pseudocode.

###Task 1: Create a Canonical Request

Create a string that includes information from your request in a standardized format. This guarantees that when the request is received by AWS, it can be calculated to the exact same signature that you had calculated in Task 3. For more information, you can see [Create a Canonical Request for Signature Version 4](https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html) for more references.

1. Define variables for the request in your application.

	```
	# HTTP verb
	method = "GET"
	# Service name
	service = "transcribe"
	# AWS Region
	region = "AWS Region"
	# Amazon Transcribe streaming endpoint
	endpoint = "https://transcribestreaming.region.amazonaws.com:8443"
	# Host
	host = "transcribestreaming.region.amazonaws.com:8443"
	# Date and time of request
	amz-date = YYYMMDD'T'HHMMSS'Z'
	# Date without time for credential scope
	datestamp = YYYYMMDD
	```

2. Create a canonical URI. The canonical URI is the part of the URI between the domain and the query
string.

	```
	canonical_uri = "/medical-stream-transcription-websocket"
	```
3. Create the canonical headers and signed headers. Note the trailing "\n" in the canonical headers.

	```
	canonical_headers = "host:" + host + "\n"
	signed_headers = "host" 
	```
4. Match the algorithm to the hashing algorithm. You must use SHA-256

	```
	algorithm = "AWS4-HMAC-SHA256"
	```
5. Create the credential scope, which scopes the derived key to the date, AWS Region, and service to which the request is made.
	
	```
	credential_scope = datestamp + "/" + region + "/" + service + "/" + "aws4_request"
	```
6. Create the canonical query string. Query string values must be URL-encoded and sorted by name.
	
	```
	canonical_querystring = "X-Amz-Algorithm=" + algorithm
	canonical_querystring += "&X-Amz-Credentials="+ access key + "/" + 	credential_scope
	canonical_querystring += "&X-Amz-Date=" + amz_date
	canonical_querystring += "&X-Amz-Expires=300"
	canonical_querystring += "&X-Amz-Security-Token=" + token
	canonical_querystring += "&X-Amz-SignedHeaders=" + signed_headers
	canonical_querystring += "&language-code=en-US&media-	encoding=pcm&sample-rate=16000"
	canonical_querystring += "&specialty=PRIMARYCARE;"
	canonical_querystring += "&type=DICTATION"
	```
7. Create a hash of the payload. For a GET request, the payload is an empty string.

	```
	payload_hash = HashSHA256(("").Encode("utf-8")).HexDigest()
	```
8. Combine all of the elements to create the canonical request

	```
	canonical_request = method + '\n'
 		+ canonical_uri + '\n'
		+ canonical_querystring + '\n'
		+ canonical_headers + '\n'
	 	+ signed_headers + '\n'
	 	+ payload_hash
	```	
	
###Task 2: Create the String to Sign

The string to sign contains meta information about your request. You use the string to sign in the next step when you calculate the request signature. For more information, see [e Create a String to Sign for
Signature Version 4](https://docs.aws.amazon.com/general/latest/gr/sigv4-create-string-to-sign.html) in the Amazon Web Services General Reference.

1. Create the string.

	```
	string_to_sign=algorithm + "\n"
		+ amz_date + "\n"
 		+ credential_scope + "\n"
 		+ HashSHA256(canonical_request.Encode("utf-8").HexDigest()
	```

###Task 3: Calculate the Signature

You derive a signing key from your AWS secret access key. For a greater protection, the derived key is specific to the date, service, and AWS Region. You use the derived key to sign the request. For more information, see [Calculate the Signature for AWS Signature Version 4](https://docs.aws.amazon.com/general/latest/gr/sigv4-calculate-signature.html) in the Amazon Web Services General Reference.

The code assumes that you have implemented the GetSignatureKey function to derive a signing key.
For more information and example functions, see [Examples of How to Derive a Signing Key for Signature Version 4](https://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html) in the Amazon Web Services General Reference.

The function HMAC(key, data) represents an HMAC-SHA256 function that returns the results in binary format.

	```
	# Create the signing key
	signing_key = GetSignatureKey(secret_key, datestamp, region, service)

	# Sign the string_to_sign using the signing key
	signature = HMAC.new(signing_key, (string_to_sign).Encode("utf-8"), 	Sha256()).HexDigest
	```

###Task 4: Add Signing Information to the Request and Create the Request URL

After you calculate the signature, add it to the query string. For more information, see [Add the Signature to the HTTP Request](https://docs.aws.amazon.com/general/latest/gr/sigv4-add-signature-to-request.html) in the Amazon Web Services General Reference.

1. Add the authentication information to the query string.

	```
	canonical_querystring += "&X-Amz-Signature=" + signature
	```

2. Create the URL for the request.

	```
	request_url = endpoint + canonical_uri + "?" + canonical_querystring
	```

## Conclusion

Congrats! You have learned how to use the request URL with your WebSocket library to make the request to the Amazon Transcribe Medical service.





